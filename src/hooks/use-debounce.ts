import { useEffect, useState } from "react";

// https://usehooks.com/useDebounce/
export function useDebounce<TValue = unknown>(
  value: TValue,
  delay: number
): TValue {
  const [debouncedValue, setDebouncedValue] = useState<TValue>(value);

  useEffect(() => {
    const handler = setTimeout(() => {
      setDebouncedValue(value);
    }, delay);

    return () => {
      clearTimeout(handler);
    };
  }, [value, delay]);

  return debouncedValue;
}
