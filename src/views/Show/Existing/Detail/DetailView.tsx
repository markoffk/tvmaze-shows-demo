import React, { useContext } from "react";
import CurrentShowIdContext from "views/Show/Existing/CurrentShowIdContext";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { StoreState } from "../../../../store/types";
import { StoreShowsItem } from "../../../../store/entities/shows/types";
import { StoreEpisodes } from "../../../../store/entities/episodes/types";
import { getEpisodesByShowIdSelector } from "../../../../store/entities/episodes/selectors";
import { Grid } from "../../../../ui-components/organisms/Grid";
import { Card } from "../../../../ui-components/molecules/Card";
import { ImageWithPlaceholder } from "../../../../ui-components/atoms/Image";

const DetailView: React.FC = () => {
  const showId = useContext<number>(CurrentShowIdContext);
  const show = useSelector<StoreState, StoreShowsItem>(
    (state) => state.shows[showId]
  );
  const showEpisodes = useSelector<StoreState, StoreEpisodes>((state) =>
    getEpisodesByShowIdSelector(state, showId)
  );

  return (
    <>
      <h1>{show.name}</h1>
      <div dangerouslySetInnerHTML={{ __html: show.summary }} />
      <div>
        <ImageWithPlaceholder
          url={show.image?.medium}
          title={show.name}
          width={210}
          height={295}
        />
      </div>
      <Grid>
        {Object.values(showEpisodes).map((episode) => {
          return (
            <Grid.Item key={`episode-${episode.id}`}>
              <Card>
                <Card.Header>
                  <Link to={`/show/${showId}/episode/${episode.id}`}>
                    S{episode.season}E{episode.number}: {episode.name}
                  </Link>
                </Card.Header>
                <Card.Body>
                  <ImageWithPlaceholder
                    url={episode.image?.medium}
                    title={episode.name}
                    width={250}
                    height={140}
                  />
                </Card.Body>
              </Card>
            </Grid.Item>
          );
        })}
      </Grid>
    </>
  );
};

export default DetailView;
