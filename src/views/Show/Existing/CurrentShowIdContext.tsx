import React from "react";

const CurrentShowIdContext = React.createContext<number>(0);

export default CurrentShowIdContext;
