import React, { useEffect } from "react";
import { Redirect, Route, Switch, Link, useRouteMatch } from "react-router-dom";
import { DetailView } from "views/Show/Existing/Detail";
import CurrentShowIdContext from "views/Show/Existing/CurrentShowIdContext";
import { useDispatch, useSelector } from "react-redux";
import { EpisodeView } from "./Episode";
import { showsFetchByIdRequest } from "../../../store/entities/shows/actions";
import { episodesFetchByShowIdRequest } from "../../../store/entities/episodes/actions";
import { StoreState } from "../../../store/types";
import { StoreShowsItem } from "../../../store/entities/shows/types";
import { BackBar } from "../../../ui-components/molecules/BackBar";

/**
 * This component guarantees that all its sub-components will have pre-loaded show info
 */
const ExistingView: React.FC = () => {
  const match = useRouteMatch<{ id: string }>();
  const dispatch = useDispatch();

  const showId = parseInt(match.params.id, 10);

  useEffect(() => {
    if (showId) {
      dispatch(showsFetchByIdRequest(showId));
      dispatch(episodesFetchByShowIdRequest(showId));
    }
  }, [dispatch, showId]);

  const show = useSelector<StoreState, StoreShowsItem | undefined>(
    (state) => state.shows[showId]
  );

  if (!show) {
    return <>loading...</>;
  }

  return (
    <CurrentShowIdContext.Provider value={showId}>
      <BackBar>
        <Link to={"/show"}>back to show list</Link>
      </BackBar>
      <Switch>
        <Route path={`${match.url}/detail`} component={DetailView} />
        <Route path={`${match.url}/episode`} component={EpisodeView} />
        <Redirect to={`${match.url}/detail`} />
      </Switch>
    </CurrentShowIdContext.Provider>
  );
};

export default ExistingView;
