import React from "react";
import { Route, Switch, useRouteMatch } from "react-router-dom";
import { ExistingView } from "./Existing";

const EpisodeView: React.FC = () => {
  const match = useRouteMatch();

  return (
    <Switch>
      <Route path={`${match.url}/:id`} component={ExistingView} />
    </Switch>
  );
};

export default EpisodeView;
