import React, { useContext } from "react";
import CurrentEpisodeIdContext from "../CurrentEpisodeIdContext";
import { useSelector } from "react-redux";
import { StoreState } from "../../../../../../store/types";
import { StoreEpisodesItem } from "../../../../../../store/entities/episodes/types";
import { Card } from "../../../../../../ui-components/molecules/Card";
import { Link } from "react-router-dom";
import { BackBar } from "../../../../../../ui-components/molecules/BackBar";
import { ImageWithPlaceholder } from "../../../../../../ui-components/atoms/Image";

const DetailView: React.FC = () => {
  const episodeId = useContext<number>(CurrentEpisodeIdContext);
  const episode = useSelector<StoreState, StoreEpisodesItem>(
    (state) => state.episodes[episodeId]
  );

  return (
    <>
      <BackBar>
        <Link to={"../.."}>back to show page</Link>
      </BackBar>
      <Card>
        <Card.Header>
          S{episode.season}E{episode.number}: {episode.name}
        </Card.Header>
        <Card.Body>
          <div dangerouslySetInnerHTML={{ __html: episode.summary }} />
          <ImageWithPlaceholder
            url={episode.image?.medium}
            title={episode.name}
            width={250}
            height={140}
          />
        </Card.Body>
      </Card>
    </>
  );
};

export default DetailView;
