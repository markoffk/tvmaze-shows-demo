import React, { useContext, useEffect } from "react";
import { Redirect, Route, Switch, useRouteMatch } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { DetailView } from "./Detail";
import CurrentEpisodeIdContext from "./CurrentEpisodeIdContext";
import { episodesFetchByIdRequest } from "../../../../../store/entities/episodes/actions";
import { StoreState } from "../../../../../store/types";
import { StoreEpisodesItem } from "../../../../../store/entities/episodes/types";
import CurrentShowIdContext from "../../CurrentShowIdContext";

const ExistingView: React.FC = () => {
  const match = useRouteMatch<{ id: string }>();
  const dispatch = useDispatch();
  const showId = useContext<number>(CurrentShowIdContext);
  const episodeId = parseInt(match.params.id, 10);

  useEffect(() => {
    (async () => {
      if (episodeId) {
        dispatch(episodesFetchByIdRequest(showId, episodeId));
      }
    })();
  }, [dispatch, showId, episodeId]);

  const episode = useSelector<StoreState, StoreEpisodesItem | undefined>(
    (state) => state.episodes[episodeId]
  );

  if (!episode) {
    return <>loading...</>;
  }

  return (
    <CurrentEpisodeIdContext.Provider value={episodeId}>
      <Switch>
        <Route path={`${match.url}/detail`} component={DetailView} />
        <Redirect to={`${match.url}/detail`} />
      </Switch>
    </CurrentEpisodeIdContext.Provider>
  );
};

export default ExistingView;
