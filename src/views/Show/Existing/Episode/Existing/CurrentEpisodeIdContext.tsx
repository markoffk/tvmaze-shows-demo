import React from "react";

const CurrentEpisodeIdContext = React.createContext<number>(0);

export default CurrentEpisodeIdContext;
