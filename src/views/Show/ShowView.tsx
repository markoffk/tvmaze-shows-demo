import React from "react";
import { Redirect, Route, Switch, useRouteMatch } from "react-router-dom";
import { ListView } from "views/Show/List";
import { ExistingView } from "views/Show/Existing";

const ShowView: React.FC = () => {
  const match = useRouteMatch();

  return (
    <Switch>
      <Route path={`${match.url}/list`} component={ListView} />
      <Route path={`${match.url}/:id`} component={ExistingView} />
      <Redirect to={`${match.url}/list`} />
    </Switch>
  );
};

export default ShowView;
