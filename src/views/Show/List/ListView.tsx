import React, { useEffect, useState } from "react";
import { Grid } from "../../../ui-components/organisms/Grid";
import { Card } from "../../../ui-components/molecules/Card";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { StoreState } from "../../../store/types";
import { StoreShows } from "../../../store/entities/shows/types";
import { showsFetchByQueryRequest } from "../../../store/entities/shows/actions";
import { useDebounce } from "../../../hooks/use-debounce";
import { ImageWithPlaceholder } from "../../../ui-components/atoms/Image";

const ListView: React.FC = () => {
  const dispatch = useDispatch();
  const [query, setQuery] = useState<string>("The Powerpuff Girls");
  const debouncedQuery = useDebounce<string>(query, 500);
  useEffect(() => {
    dispatch(showsFetchByQueryRequest(debouncedQuery));
  }, [dispatch, debouncedQuery]);

  const shows = useSelector<StoreState, StoreShows>((state) => state.shows);

  return (
    <>
      <input
        type="text"
        value={query}
        placeholder="type show name..."
        onChange={(e) => {
          setQuery(e.currentTarget.value);
        }}
      />
      <Grid>
        {Object.values(shows).map((show) => {
          return (
            <Grid.Item key={`show-${show.id}`}>
              <Card>
                <Card.Header>
                  <Link to={`/show/${show.id}`}>{show.name}</Link>
                </Card.Header>
                <Card.Body>
                  <ImageWithPlaceholder
                    url={show.image?.medium}
                    title={show.name}
                    width={210}
                    height={295}
                  />
                </Card.Body>
              </Card>
            </Grid.Item>
          );
        })}
      </Grid>
    </>
  );
};

export default ListView;
