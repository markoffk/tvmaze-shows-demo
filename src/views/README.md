View components are organized in hierarchical way to follow route path.
Views are glue between route, state and presentation.
For example, what can do view is:

1. Dispatch actions
2. Ensure there is data in state
3. Render content conditionally, using `atomic` components

View itself should not contain any styling, as it is a role of Atomic components.
It is also better to avoid usage of raw html tags, because if you will need to
style them (and you will, most likely) - nevertheless you will end up with need to create some atomic
component for this purpose.
