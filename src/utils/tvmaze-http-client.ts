const fetchJsonData = async (url: string) => {
  return (await fetch(`http://api.tvmaze.com${url}`)).json();
};

class TvmazeHttpClient {
  async getShowsByQuery(query: string) {
    return fetchJsonData(`/search/shows?q=${query}`);
  }

  async getShowById(showId: number) {
    return fetchJsonData(`/shows/${showId}`);
  }

  async getShowEpisodes(showId: number) {
    return fetchJsonData(`/shows/${showId}/episodes`);
  }

  async getEpisodeById(episodesId: number) {
    return fetchJsonData(`/episodes/${episodesId}`);
  }
}

export const tvmazeHttpClient = new TvmazeHttpClient();
