import React from "react";
import { Redirect, Route, Switch, Link } from "react-router-dom";
import ShowView from "views/Show/ShowView";
import { MainTemplate } from "./ui-components/templates";
import { Header } from "./ui-components/organisms/Header";

function App() {
  return (
    <MainTemplate
      header={
        <Header>
          <Link to={"/"}>Home</Link>
        </Header>
      }
      body={
        <Switch>
          <Route path="/show" component={ShowView} />
          <Redirect to={`/show`} />
        </Switch>
      }
    />
  );
}

export default App;
