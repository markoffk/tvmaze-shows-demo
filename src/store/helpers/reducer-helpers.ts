import * as R from "ramda";

type Identifiable = {
  id: number;
};

/**
 * Converts array to hashmap using "id" attribute and merges with previous data.
 * For example it can convert [{id: 123, name: "a"}, {id: 456, name: "b"}]
 * to {123: {id: 123, name: "a"}, 456: {id: 456, name: "b"}}
 */
export const mergeAndIndexById = <TItem extends Identifiable>(
  items: TItem[],
  previousState: any = {}
): Record<number, TItem> => {
  return R.compose(
    R.indexBy((item: Identifiable) => item.id.toString()),
    R.map((item: Identifiable) => ({
      ...(previousState[item.id] ? previousState[item.id] : {}),
      ...item,
    }))
  )(items) as Record<number, TItem>; // todo remove type assertion
};
