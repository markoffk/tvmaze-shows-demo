import { applyMiddleware, compose, createStore } from "redux";
import createSagaMiddleware from "redux-saga";
import createRootReducer from "./createRootReducer";
import {
  episodesFetchByIdSaga,
  episodesFetchByShowIdSaga,
} from "./entities/episodes/sagas";
import {
  showsFetchByIdSaga,
  showsFetchByQuerySaga,
} from "./entities/shows/sagas";

const initialState = {};
const enhancers = [];

const sagaMiddleware = createSagaMiddleware();

if (process.env.NODE_ENV === "development") {
  const devToolsExtension = (window as any).__REDUX_DEVTOOLS_EXTENSION__;

  if (typeof devToolsExtension === "function") {
    enhancers.push(devToolsExtension());
  }
}

const store = createStore(
  createRootReducer(),
  initialState,
  compose(applyMiddleware(sagaMiddleware), ...enhancers)
);

sagaMiddleware.run(episodesFetchByShowIdSaga);
sagaMiddleware.run(episodesFetchByIdSaga);
sagaMiddleware.run(showsFetchByIdSaga);
sagaMiddleware.run(showsFetchByQuerySaga);

export default store;
