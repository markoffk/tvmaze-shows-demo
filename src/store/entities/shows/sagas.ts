import { call, put, takeEvery } from "redux-saga/effects";
import { ShowsAction } from "./actions";
import { tvmazeHttpClient } from "../../../utils/tvmaze-http-client";
import { ActionWithPayload } from "../../types";

export function* showsFetchByQuerySaga() {
  yield takeEvery(
    ShowsAction.FetchByQueryRequest,
    function* (action: ActionWithPayload<ShowsAction, string>): any {
      try {
        yield put({
          type: ShowsAction.FetchByQuerySucceeded,
          payload: yield call<any>(
            tvmazeHttpClient.getShowsByQuery,
            action.payload
          ) as any,
        });
      } catch (e) {
        yield put({ type: ShowsAction.FetchByQueryFailed, message: e.message });
      }
    }
  );
}

export function* showsFetchByIdSaga() {
  yield takeEvery(
    ShowsAction.FetchByIdRequest,
    function* (action: ActionWithPayload<ShowsAction, number>): any {
      try {
        yield put({
          type: ShowsAction.FetchByIdSucceeded,
          payload: yield call<any>(
            tvmazeHttpClient.getShowById,
            action.payload
          ) as any,
        });
      } catch (e) {
        yield put({ type: ShowsAction.FetchByIdFailed, message: e.message });
      }
    }
  );
}
