export enum ShowsAction {
  FetchByQueryRequest = "[shows] fetch by query request",
  FetchByQuerySucceeded = "[shows] fetch by query succeeded",
  FetchByQueryFailed = "[shows] fetch by query failed",
  FetchByIdRequest = "[shows] fetch by id request",
  FetchByIdSucceeded = "[shows] fetch by id succeeded",
  FetchByIdFailed = "[shows] fetch by id failed",
}

export const showsFetchByQueryRequest = (query: string) => ({
  type: ShowsAction.FetchByQueryRequest,
  payload: query,
});

export const showsFetchByIdRequest = (showId: number) => ({
  type: ShowsAction.FetchByIdRequest,
  payload: showId,
});
