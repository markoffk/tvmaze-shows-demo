import { ShowsAction } from "store/entities/shows/actions";
import { ActionWithPayload } from "store/types";
import { StoreShows, StoreShowsItem } from "store/entities/shows/types";
import { mergeAndIndexById } from "store/helpers/reducer-helpers";
import { Reducer } from "redux";

const initialState: StoreShows = {};

export const showsReducer: Reducer<StoreShows> = (
  state: StoreShows = initialState,
  { type, payload }: ActionWithPayload<ShowsAction>
) => {
  switch (type) {
    case ShowsAction.FetchByQuerySucceeded:
      const shows = payload as { score: number; show: StoreShowsItem }[];

      return {
        ...state,
        ...mergeAndIndexById(
          shows.map((item) => item.show),
          state
        ),
      };

    case ShowsAction.FetchByIdSucceeded:
      return {
        ...state,
        [payload.id]: payload,
      };

    default:
      return state;
  }
};
