export type StoreShowsItem = {
  id: number;
  name: string;
  summary: string;
  image?: {
    medium: string;
    original: string;
  };
};

export type StoreShows = { [key: string]: StoreShowsItem };
