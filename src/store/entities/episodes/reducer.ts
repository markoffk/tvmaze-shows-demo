import { EpisodesAction } from "store/entities/episodes/actions";
import { ActionWithPayload } from "store/types";
import {
  StoreEpisodes,
  StoreEpisodesItem,
} from "store/entities/episodes/types";
import { mergeAndIndexById } from "store/helpers/reducer-helpers";
import { Reducer } from "redux";

const initialState: StoreEpisodes = {};

export const episodesReducer: Reducer<StoreEpisodes> = (
  state: StoreEpisodes = initialState,
  { type, payload }: ActionWithPayload<EpisodesAction>
) => {
  switch (type) {
    case EpisodesAction.FetchByShowIdSucceeded:
      const { episodes, showId } = payload as {
        showId: number;
        episodes: StoreEpisodesItem[];
      };

      return {
        ...state,
        ...mergeAndIndexById(
          episodes.map((episode) => ({ ...episode, showId })),
          state
        ),
      };

    case EpisodesAction.FetchByIdSucceeded:
      return {
        ...state,
        [payload.episode.id]: {
          ...payload.episode,
          showId: payload.showId,
        },
      };

    default:
      return state;
  }
};
