export type StoreEpisodesItem = {
  id: number;
  name: string;
  season: number;
  number: number;
  summary: string;
  image?: {
    medium: string;
    original: string;
  };
  showId: number;
};

export type StoreEpisodes = { [key: string]: StoreEpisodesItem };
