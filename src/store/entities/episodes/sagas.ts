import { call, put, takeEvery } from "redux-saga/effects";
import { EpisodesAction } from "./actions";
import { ActionWithPayload } from "../../types";
import { tvmazeHttpClient } from "../../../utils/tvmaze-http-client";

export function* episodesFetchByShowIdSaga() {
  yield takeEvery(
    EpisodesAction.FetchByShowIdRequest,
    function* (action: ActionWithPayload<EpisodesAction, number>): any {
      try {
        yield put({
          type: EpisodesAction.FetchByShowIdSucceeded,
          payload: {
            showId: action.payload,
            episodes: yield call<any>(
              tvmazeHttpClient.getShowEpisodes,
              action.payload
            ) as any,
          },
        });
      } catch (e) {
        yield put({
          type: EpisodesAction.FetchByShowIdFailed,
          message: e.message,
        });
      }
    }
  );
}

export function* episodesFetchByIdSaga() {
  yield takeEvery(
    EpisodesAction.FetchByIdRequest,
    function* (
      action: ActionWithPayload<
        EpisodesAction,
        { showId: number; episodeId: number }
      >
    ): any {
      try {
        yield put({
          type: EpisodesAction.FetchByIdSucceeded,
          payload: {
            showId: action.payload?.showId,
            episode: yield call<any>(
              tvmazeHttpClient.getEpisodeById,
              action.payload?.episodeId
            ) as any,
          },
        });
      } catch (e) {
        yield put({ type: EpisodesAction.FetchByIdFailed, message: e.message });
      }
    }
  );
}
