export enum EpisodesAction {
  FetchByShowIdRequest = "[episodes] fetch by show id request",
  FetchByShowIdSucceeded = "[episodes] fetch by show id succeeded",
  FetchByShowIdFailed = "[episodes] fetch by show id failed",
  FetchByIdRequest = "[episodes] fetch by id request",
  FetchByIdSucceeded = "[episodes] fetch by id succeeded",
  FetchByIdFailed = "[episodes] fetch by id failed",
}

export const episodesFetchByShowIdRequest = (showId: number) => ({
  type: EpisodesAction.FetchByShowIdRequest,
  payload: showId,
});

export const episodesFetchByIdRequest = (
  showId: number,
  episodeId: number
) => ({
  type: EpisodesAction.FetchByIdRequest,
  payload: { episodeId, showId },
});
