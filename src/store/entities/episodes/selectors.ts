import * as R from "ramda";
import { StoreState } from "store/types";
import { createSelector } from "reselect";

const getEpisodes = (state: StoreState) => state.episodes;

export const getEpisodesByShowIdSelector = createSelector(
  getEpisodes,
  (_: StoreState, showId: number) => showId,
  (episodes, showId) => {
    return R.filter<any>(R.propEq("showId", showId), episodes);
  }
);
