Here all application state management things should be placed.
Each folder inside `entities/` represents separate key in application redux state.

For each of folder inside `entities/` you can find:

1. action creators (`actions.ts`)
2. reducers (`reducer.ts`)
3. sagas (`sagas.ts`)
4. typings for state objects (`types.ts`)
5. `reselect` selectors (`selectors.ts`)
