import { combineReducers } from "redux";

import { episodesReducer } from "./entities/episodes/reducer";
import { showsReducer } from "store/entities/shows/reducer";

const createRootReducer = () =>
  combineReducers({
    episodes: episodesReducer,
    shows: showsReducer,
  });

export default createRootReducer;
