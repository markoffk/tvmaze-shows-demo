import { Action } from "redux";
import { StoreShows } from "store/entities/shows/types";
import { StoreEpisodes } from "./entities/episodes/types";

export type ActionWithPayload<
  TAction = any,
  TPayload = any
> = Action<TAction> & {
  payload?: TPayload;
};

export type StoreState = {
  episodes: StoreEpisodes;
  shows: StoreShows;
};
