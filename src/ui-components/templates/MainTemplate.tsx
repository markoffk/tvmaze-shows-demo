import React from "react";
import styled from "styled-components";

const StyledMainTemplate = styled.div`
  .main-template-header {
    padding: 10px;
  }
  .main-template-body {
    padding: 10px;
  }
`;

type Props = {
  header?: JSX.Element | string;
  body: JSX.Element | string;
};

const MainTemplate: React.FC<Props> = ({ header, body }) => {
  return (
    <StyledMainTemplate className="main-template">
      {header && <div className="main-template-header">{header}</div>}
      <div className="main-template-body">{body}</div>
    </StyledMainTemplate>
  );
};

export default MainTemplate;
