import React from "react";
import styled from "styled-components";

const StyledCardBody = styled.div``;

const CardBody: React.FC = ({ children }) => {
  return <StyledCardBody className="card-body">{children}</StyledCardBody>;
};

export default CardBody;
