import React from "react";
import styled from "styled-components";

const StyledCardHeader = styled.div``;

const CardHeader: React.FC = ({ children }) => {
  return (
    <StyledCardHeader className="card-header">{children}</StyledCardHeader>
  );
};

export default CardHeader;
