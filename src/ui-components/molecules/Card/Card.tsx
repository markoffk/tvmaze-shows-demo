import React from "react";
import styled from "styled-components";
import CardHeader from "./CardHeader";
import CardBody from "./CardBody";

const StyledCard = styled.div`
  padding: 5px;

  .card-header {
    padding: 5px;
  }
  .card-body {
    padding: 5px;
  }
`;

type StaticProps = {
  Header: typeof CardHeader;
  Body: typeof CardBody;
};

const Card: React.FC & StaticProps = ({ children }) => {
  return <StyledCard className="card">{children}</StyledCard>;
};

Card.Header = CardHeader;
Card.Body = CardBody;

export default Card;
