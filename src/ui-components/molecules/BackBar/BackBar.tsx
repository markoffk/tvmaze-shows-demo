import React from "react";
import styled from "styled-components";

const StyledBackBar = styled.div`
  padding: 10px;
  background-color: #eee;
`;

const BackBar: React.FC = ({ children }) => {
  return <StyledBackBar className="back-bar">{children}</StyledBackBar>;
};

export default BackBar;
