import React from "react";

type Props = {
  width: number;
  height: number;
  title: string;
};

const PlaceholderImage: React.FC<Props> = ({ width, height, title }) => {
  return (
    <img
      src={`https://via.placeholder.com/${width}x${height}?text=no+image`}
      alt={title}
    />
  );
};

export default PlaceholderImage;
