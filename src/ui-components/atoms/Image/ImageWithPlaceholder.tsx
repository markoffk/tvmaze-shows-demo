import React from "react";
import { PlaceholderImage } from "./index";

type Props = {
  url?: string;
  title: string;
  width: number;
  height: number;
};

const ImageWithPlaceholder: React.FC<Props> = ({
  url,
  title,
  width,
  height,
}) => {
  return url ? (
    <img src={url} alt={title} width={width} height={height} />
  ) : (
    <PlaceholderImage width={width} height={height} title={title} />
  );
};

export default ImageWithPlaceholder;
