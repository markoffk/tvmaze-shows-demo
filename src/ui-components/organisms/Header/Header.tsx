import React from "react";
import styled from "styled-components";

const StyledHeader = styled.div`
  padding: 10px;
  background-color: #ccccff;
`;

const Header: React.FC = ({ children }) => {
  return <StyledHeader className="header">{children}</StyledHeader>;
};

export default Header;
