import React from "react";
import styled from "styled-components";

const StyledGridItem = styled.div``;

const GridItem: React.FC<
  React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement>
> = ({ children, ...rest }) => {
  return (
    <StyledGridItem className={"grid-item"} {...(rest as any)}>
      {children}
    </StyledGridItem>
  );
};

GridItem.defaultProps = {};

export default GridItem;
