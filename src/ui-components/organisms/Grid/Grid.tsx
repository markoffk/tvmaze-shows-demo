import React from "react";
import styled from "styled-components";
import GridItem from "./GridItem";

type StaticProps = {
  Item: typeof GridItem;
};

const StyledGrid = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  align-items: flex-end;

  .grid-item {
    flex-basis: 20%;
  }

  @media (max-width: 479px) {
    flex-direction: column;
    align-items: flex-start;
    .grid-item {
      flex-grow: 1;
      flex-basis: auto;
    }
  }
`;

const Grid: React.FC<
  React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement>
> &
  StaticProps = ({ children, ...rest }) => {
  return <StyledGrid {...(rest as any)}>{children}</StyledGrid>;
};

Grid.Item = GridItem;

export default Grid;
